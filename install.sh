#!/bin/bash

#	Run this script to install the Revolt system
#	author: Tom 'SpuQ' Santens
#	date: 09/12/2016
#
#	TODO untested!

# check whether script is running as root
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

echo "+++ installing REVOLT"

# change directory to the directory of this script
echo "++ changing working directory to $(dirname "$0")";
cd "$(dirname "$0")"

# check distro

## Intel Edison - poky
if [[ $(uname -a | grep poky &>/dev/null && echo 1 || echo 0) -ne 0 ]]; then
	DISTRO="Edison";

	echo "++ checking for node.js"
	if [[ $(which nodejs &>/dev/null && echo 1 || echo 0) -ne 0 ]]; then
		echo "+ node.js is already installed";
	else
		echo "+ installing node.js for $DISTRO";
		#TODO
	fi

	echo "++ checking for nodeRED"
	if [[ $(which node-red &>/dev/null && echo 1 || echo 0) -ne 0 ]]; then
		echo "+ nodeRED is already installed";
	else
		echo "+ installing nodeRED for $DISTRO";
		#TODO
	fi

	# make symlink '/usr/bin/nodejs' to /bin/node ?
	ln -s "$(which node)" /usr/bin/nodejs

	# at - cron jobs - currently required for the Qcom driver
	opkg install at

## Ubuntu
elif [[ $(uname -a | grep Ubuntu &>/dev/null && echo 1 || echo 0) -ne 0 ]]; then
	DISTRO="Ubuntu system";

	echo "++ checking for node.js"
	if [[ $(which nodejs &>/dev/null && echo 1 || echo 0) -ne 0 ]]; then
		echo "+ node.js is already installed";
	else
		echo "+ installing node.js for $DISTRO";
		apt-get install nodejs
	fi

	echo "++ checking for nodeRED"
	if [[ $(which node-red &>/dev/null && echo 1 || echo 0) -ne 0 ]]; then
		echo "+ nodeRED is already installed";
	else
		echo "+ installing nodeRED for $DISTRO";
		npm install -g node-red
	fi

	# at - cron jobs - currently required for the Qcom driver
	apt-get install at

## Raspberry Pi - jessie (armv7l)
elif [[ $(uname -a | grep armv7l &>/dev/null && echo 1 || echo 0) -ne 0 ]]; then
	DISTRO="Laika-pochw";
	
	echo "+ checking for npm"
	if [[ $(which npm &>/dev/null && echo 1 || echo 0) -ne 0 ]]; then
		echo "npm is already installed"
	else
		apt-get -y install npm
	fi

	echo "+ checking for node.js"
	if [[ $(which nodejs &>/dev/null && echo 1 || echo 0) -ne 0 ]]; then
		echo "node.js is already installed"
	else
		curl -sL https://deb.nodesource.com/setup_4.x | sudo bash -
		apt-get -y install nodejs
	fi

	echo "++ checking for nodeRED"
	if [[ $(which node-red &>/dev/null && echo 1 || echo 0) -ne 0 ]]; then
		echo "+ nodeRED is already installed";
	else
		echo "+ installing nodeRED for $DISTRO";
		npm install -g node-red
	fi

	# at - cron jobs - currently required for the Qcom driver
	apt-get -y install at

## No known distro
else
	echo "this distro is not supported, exit installation"
	exit 1;
fi


echo "++ Setting up node-red service (init.d)"
cp ./system/init.d/node-red /etc/init.d/
update-rc.d node-red defaults

echo "++ Setting up Revolt service (init.d)"
cp ./system/init.d/revolt /etc/init.d/
update-rc.d revolt defaults

echo "++ Installing Qcom system"
sh ./Qcom/QdriverV2.1/install_Qcom.sh

echo "++ Installing Revolt configuration"

echo "+ creating revolt directory in /opt/"
mkdir /opt/revolt/

echo "+ creating config directory in /opt/revolt/"
mkdir /opt/revolt/config/

echo "+ copying system files to /opt/revolt/config/"
cp -r ./config/* /opt/revolt/config/

echo "+ creating revolt directory in /var/"
mkdir /var/revolt/

echo "+ creating directory for persistent configurations /var/revolt/config/"
mkdir /var/revolt/config/

echo "+ installing config defaults to /var/revolt/config/"
cp ./system/config_defaults/* /var/revolt/config/

echo "+ creating image directory in /opt/revolt"
mkdir /opt/revolt/images/

echo "+ copying images to /opt/revolt/image/"
cp ./images/* /opt/revolt/images/

echo "+ copying uninstall script to /opt/revolt/"
cp uninstall.sh /opt/revolt/


echo "++ Installing Networking stuff"

echo "+ installing hostapd (for access point)"
apt-get install hostapd

echo "+ installing dhcp server (for access point)"
apt-get install isc-dhcp-server

echo "+ copying interfaces file to /etc/network/"
cp ./system/interfaces /etc/network/


echo "++ Install Revolt's NodeRED stuff"

echo "+ creating app directory in /opt/revolt/"
mkdir /opt/revolt/app/

echo "+ copying version to /opt/revolt/"
cp version /opt/revolt/

echo "+ copying nodeRED files to /opt/revolt/app/"
cp -r ./app/* /opt/revolt/app/

echo "+ installing Revolt node dependencies (express, body-parser, ...)"
cd /opt/revolt/app/
npm install
cd "$(dirname "$0")"

echo "+ creating app-vars directory in /var/revolt/"
mkdir /var/revolt/app-vars/

echo "+ creating flows directory in /var/revolt/"
mkdir /var/revolt/flows/


echo "++ preparing Revolt application webUI"

echo "+ creating appui directory in /var/revolt/"
mkdir /var/revolt/appui/

echo "+ installing default application UI"
cp ./appui/index.html /var/revolt/appui/

echo "+ installing node packages"
cd /opt/revolt/config/
npm install
cd "$(dirname "$0")"

echo "+ setting platform identifier in /opt/revolt/platform"
touch /opt/revolt/platform
echo $DISTRO > /opt/revolt/platform

echo "done"
exit 0;
