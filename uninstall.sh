#!/bin/sh

# check whether script is running as root
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

echo "++ uninstalling Revolt"

echo "+ uninstalling and removing services (init.d)"
rm /etc/init.d/node-red
rm /etc/init.d/revolt
update-rc.d node-red remove
update-rc.d revolt remove

# Uninstall Qcom is other script in /opt/Qcom/
echo "+ removing Qcom"
sh /opt/Qcom/uninstall.sh

echo "+ removing /var/revolt/ and its content"
rm -rf /var/revolt/

echo "+ removing log-files (/var/log/)"
rm /var/log/node-red.log

echo "+ removing /opt/revolt/ and its content"
rm -rf /opt/revolt/

echo "done - it should be all gone"
exit 0;
