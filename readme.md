![Revolt](images/revolt_scribble_black.png)

# A nerve system for smart things
TODO: Revolt is...
![ConfigUI](images/revolt-scheme.png)
A computer system for embedding intelligence.
## Installing Revolt
Download and install the Revolt package on the linux host (e.g., Raspberry Pi)
```
$ wget https://gitlab.com/SpuQ/revolt-core/repository/archive.tar?ref=master
$ tar -xf revolt-core*.tar && cd revolt-core*
$ sudo ./install.sh
$ sudo shutdown -r now
```


After rebooting, the Revolt host system will be configured as wireless access point (e.g. revolt-xxxx). Connect to this network and
in your favorite browser go to http://10.30.40.1:3000. Now you should see something like this:
  
![ConfigUI](images/revolt-config-ui.png)

## Tutorials
In the near future, I will start posting tutorials!
+ Getting started
+ Creating Hardware Modules
+ Creating Software Modules

## License
This software is free for personal use. For commercial use, contact the author.
