/* 
 *	networking.js
 *	networking module for revolt core
 *
 *	written on 23/01/2017 by Tom 'SpuQ' Santens	
 */

module.exports = function(app) {	//TODO there are cleaner ways than passing 'app' around...

	WirelessInterface = require('./wirelessInterface');
	Storage = require('./storage');

	var exec = require('child_process').exec;

	var scriptsPath = "./scripts/wireless_client/";

	// create storage objects (data persistency)
	var wap_settings_storage = new Storage("wap_settings");
	var wclient_settings_storage = new Storage("wclient_settings");
	var networking_settings_storage = new Storage("networking_settings");

	// variables for within the program
	var wap_settings;
	var wclient_settings;
	var networking_settings;

	// create wireless interface object
	var wlan0;


	initialize_networking();

	/*
	 *	webAPI routes
	 */

	// get networking settings
	app.get('/api/networking/networking_settings',function(req, res){
		res.setHeader('Content-Type', 'application/json');
		res.send( getNetworkingSettings() );
	});
	
	// post networking settings
	app.post('/api/networking/networking_settings',function(req, res){
		var settings = req.body;
		res.setHeader('Content-Type', 'application/json');

		// debug Boolean Bug
		console.log("received settings:\n"+JSON.stringify(settings));

		if( setNetworkingSettings(settings) ){
			res.send('{"msg":"networking settings saved"}');
		}
		else{
			res.send('{"msg":"error; settings not saved"}');
		}
	});


	// get wireless client status
	app.get('/api/networking/wclient_status', function(req, res){
		res.setHeader('Content-Type', 'application/json');
		res.send( getClientStatus() );
	});
	
	// get wireless client settings
	app.get('/api/networking/wclient_settings',function(req, res){
		res.setHeader('Content-Type', 'application/json');
		res.send( getClientSettings() );
	});

	// post wireless client settings
	app.post('/api/networking/wclient_settings',function(req, res){
		var settings = req.body;
		console.log(settings);

		res.setHeader('Content-Type', 'application/json');

		if( setClientSettings( settings ) ) {
			res.send('{"msg":"wireless client settings saved"}');
		}
		else {
			res.send('{"msg":"error; settings not saved!"}');
		}

		// TODO apply new settings if client is active
	});


	// get wireless access point status
	app.get('/api/networking/wap_status', function(req, res){
		res.setHeader('Content-Type', 'application/json');
		res.send( getAccessPointStatus() );
	});

	// get wireless access point settings
	app.get('/api/networking/wap_settings',function(req, res){
		res.setHeader('Content-Type', 'application/json');
		res.send( getAccessPointSettings() );
	});
	
	// post wireless access point settings
	app.post('/api/networking/wap_settings',function(req, res){
		var settings = req.body;

		if( setAccessPointSettings( settings ) ) {
			res.send('{"msg":"wireless access point settings saved"}');
		}
		else {
			res.send('{"msg":"error; wap settings not saved!"}');
		}
	});


	// get list of available networks [works]
	app.get('/api/networking/available_networks',function(req, res){
		wlan0.getAvailableNetworks( function(networks){
			res.setHeader('Content-Type', 'application/json');
			res.send(networks);
		});
	});


	/*	Networking management
	 *	
	 */
	
	function initialize_networking(){
		// load saved settings from files
		loadClientSettings();
		loadNetworkingSettings();
		loadAccessPointSettings();

		// create wireless object
		wlan0 = new WirelessInterface("wlan0");

		// start networking based on 'networking settings'
		modeManager();
	}


	/*
	 *	functions
	 */
// boolean bug change
	function loadNetworkingSettings(){
		networking_settings = JSON.parse(networking_settings_storage.load());
		console.log(networking_settings);
	}

	function saveNetworkingSettings(){
		console.log(networking_settings);
		networking_settings_storage.save(JSON.stringify(networking_settings));
	}
// end change

	function loadClientSettings(){
		wclient_settings = JSON.parse(wclient_settings_storage.load());
	}

	function saveClientSettings(){
		wclient_settings_storage.save(JSON.stringify(wclient_settings));
	}


	function loadAccessPointSettings(){
		wap_settings = JSON.parse(wap_settings_storage.load());
	}

	function saveAccessPointSettings(){
		wap_settings_storage.save(JSON.stringify(wap_settings));
	}


	/*	setClientSettings
	 *	saves the new settings locally and in storage
	 *	does not configure the interface itself!
	 *	TODO cleanup #Dirty	
	 */
	function setClientSettings( settings ){
		// check minimum required data
		if(typeof settings.ssid == "undefined" ) return false;
		if(typeof settings.security == "undefined" ) return false;
		if(typeof settings.static == "undefined" ) return false;

		// check valid data configuration for network
		if( settings.security == "none" ){
			settings.passwd = "";
		}
		else {
			if(typeof settings.passwd == "undefined") return false;
		}

		// check valid data configuration for interface
		if( settings.static == true ){
			if(typeof settings.address == "undefined") return false;
			if(typeof settings.netmask == "undefined") return false;
		}
		else {
			settings.address = "";
			settings.netmask = "";
		}

		// locally save settings
		wclient_settings.security = settings.security;
		wclient_settings.passwd = settings.passwd;
		wclient_settings.ssid = settings.ssid;
		wclient_settings.static = settings.static;
		wclient_settings.address = settings.address;
		wclient_settings.netmask = settings.netmask;
		
		// store settings
		saveClientSettings();

		return true;
	}


	/* 	getClientSettings
	 *
	 */
	function getClientSettings(){
		var settings = wclient_settings;

		// if client is active and static == false, get ip and netmask from interface
		if(!settings.static){
			var temp = wlan0.getInterfaceConfiguration();
			if(typeof temp.address != "undefined") settings.address = temp.address;
			if(typeof temp.netmask != "undefined") settings.netmask = temp.netmask;
		}

		return settings;
	}


	/*	enableClient
	 *	pushes the client settings to the interface
	 */
	function enableClient(){
		wlan0.setClient( wclient_settings );
	}


	/*	enableAccessPoint
	 *	pushes the access point settings to the interface
	 */
	function enableAccessPoint(){
		wlan0.setAccessPoint( wap_settings );
	}


	/*	setNetworkingSettings
	 *	saves the new settings locally and in storage
	 */
	function setNetworkingSettings( settings ){
		if(typeof settings.wap_enabled == "undefined" ) return false;
		if(typeof settings.wap_if_unavailable == "undefined" ) return false;

		networking_settings.wap_if_unavailable = settings.wap_if_unavailable;
		networking_settings.wap_enabled = settings.wap_enabled;
		
		saveNetworkingSettings();

		return true;
	}


	/* 	getNetworkingSettings
	 *
	 */
	function getNetworkingSettings(){
		var settings = {};

		 settings.wap_if_unavailable = networking_settings.wap_if_unavailable;
		 settings.wap_enabled = networking_settings.wap_enabled;

		return settings;
	}
	
	
	function getClientStatus(){
		var data = {};
		status = wlan0.getClientStatus().split('\n');
		
		if(status[0]=="disconnected"){
			data.status=status[0];
			data.substatus="configured for "+wclient_settings.ssid;
		}
		else {
			data.status = status[0];
			data.substatus = status[1];
		}

		return data;
	}


	/*	setAccessPointSettings
	 *	saves the new settings locally and in storage
	 *	does not configure the interface itself!
	 *	TODO cleanup #Dirty	
	 */
	function setAccessPointSettings( settings ){
		// check minimum required data
		if(typeof settings.ssid == "undefined" ) return false;
		if(typeof settings.security == "undefined" ) return false;
		//if(typeof settings.address == "undefined") return false;	// TODO: future
		//if(typeof settings.netmask == "undefined") return false;

		// check valid data configuration for network
		if( settings.security == "none" ){
			settings.passwd = "";
		}
		else {
			if(typeof settings.passwd == "undefined") return false;
		}

		// locally save settings
		wap_settings.security = settings.security;
		wap_settings.passwd = settings.passwd;
		wap_settings.ssid = settings.ssid;
		//wap_settings.address = settings.address;
		//wap_settings.netmask = settings.netmask;
		
		// store settings
		saveAccessPointSettings();

		return true;
	}


	/* 	getAccessPointSettings
	 *
	 */
	function getAccessPointSettings(){
		var settings = wap_settings;
		return settings;
	}

	function getAccessPointStatus(){
		var data = {};
		var status = wlan0.getAccessPointStatus().split('\n');
		
		if(status == "inactive"){
			data.status = status[0];
			data.substatus = "configured for "+wap_settings.ssid;
		}
		else{
			data.status = status[0];
			data.substatus = status[1];
		}

		return data;
	}


	/*	
	 *
	 */
	function modeManager(){
		// start networking based on 'networking settings'
		if( typeof networking_settings != "undefined" ){
			if(networking_settings.wap_enabled ){
				// initialize access point
				console.log("initializing access point");
				enableAccessPoint();
			}
			else if(networking_settings.wap_if_unavailable){
				console.log("initializing client, check after 5 mins");
				// initialize client, enable accesspoint if network not available after 5 minutes
				enableClient();
				setTimeout(function(){
					console.log("5 mins passed, checking client connection");
					var clientStatus = wlan0.getClientStatus().split('\n');
					if( clientStatus[0] != "connected" ){	
						console.log("client not connected, starting access point");
						enableAccessPoint();
					}
					else {
						console.log("client is connected");
					}
				}, 300000); // 5mins
			}
			else {
				// enable client
				console.log("starting client");
				enableClient();
			}
		}
		else {
			// TODO fallback mode
			console.log("no settings found, starting fallback");
		}
	}
}
