/* 
 *	storage.js
 *
 *	written on 24/01/2017 by Tom 'SpuQ' Santens	
 */


var storagePath = "/var/revolt/config/";
var fs = require('fs');

module.exports = Storage;

function Storage( variable ){
	if(typeof variable == "undefined") return -1;
	var path = storagePath+variable;

	init();

	function init(){
		// check if file exists, if not create it
		if( !fs.existsSync(path) ){
			console.log("creating storage file '"+path+"'");
			fs.closeSync(fs.openSync(path, 'w'));
			save("{}");
		}
	}


	/*	
	 */
	function save( value ){
		fs.writeFileSync(path, value);
	}

	this.save = save;


	/*	
	 */
	function load(){
		var value = fs.readFileSync(path, "utf8");
		console.log("read from file: "+value);
		return value;
	}

	this.load = load;

	/*
	 *
	 */
	function remove( callback ){
		fs.unlinkSync(path);
	}

	this.remove = remove;	
}
