/* 
 *	revolt-system.js
 *	Functions for the revolt system
 *	
 *	written on 12/05/2017 by Tom 'SpuQ' Santens	
 */

var exec = require('child_process').exec;
var fs = require('fs');

var scriptsPath = "/opt/revolt/config/scripts/";	// absolute path to revolt scripts folder

module.exports = {
	/*
	 *	System Power
	 */

	// shutdown the system
	shutdown: function( callback ){
		exec('sh '+scriptsPath+'shutdown.sh');
	},

	// reboot the system
	restart: function( callback ){
		exec('sh '+scriptsPath+'reboot.sh');
	},


	/*
	 *	System Time
	 */

	// set the system time
	// TODO
	setTime: function( data, callback ){

	},

	// get the system time
	getTime: function( callback ){
		exec('sh '+scriptsPath+'systime.sh', function(err, stdout, stderr){
			var temp = {};
			temp.systime = stdout;
			
			if(typeof callback == "function"){
				callback(temp);
			}
		});
	},

	// get the system timezone
	getTimezone: function( callback ){

	},

	// set the system timezone
	setTimezone: function( callback ){

	},

	// get the system uptime
	getUptime: function( callback ){
		exec('sh '+scriptsPath+'uptime.sh', function(err, stdout, stderr){
			var temp = {};
			temp.uptime = stdout;

			if(typeof callback == "function"){
				callback(temp);
			}
		});
	},


	/*
	 *	System Connectivity
	 */

	// check if the computer is connected to the internet
	getNetworkStatus: function(){

	},


	/*
	 *	System Resources
	 */

	// check usage of storage space
	// TODO
	getStorage: function( callback ){
		if(typeof callback == "function"){
			callback( JSON.parse('{ "storage" : "256GB" }') );
		}
	},

	// check the RAM usage
	// TODO
	getRam: function( callback ){

	},

	// check the CPU usage
	// TODO
	getCpu: function( callback ){

	},


	/*
	 *	Revolt System
	 */

	// install revolt system firmware
	installFirmware: function(){

	},

	// firmware version
	firmwareVersion: function( callback ){
		var version = fs.readFileSync('/opt/revolt/version', 'utf8');
		var temp = {};
		temp.firmware = version;

		if(typeof callback == "function"){
			callback(temp);
		}
	},

	// hardware platform
	hardwarePlatform: function( callback ){
		var platform = fs.readFileSync('/opt/revolt/platform', 'utf8');
		var temp = {};
		temp.platform = platform;

		if(typeof callback == "function"){
			callback(temp);
		}
	},

	/*
	 *	Revolt Application
	 */

	// install a webUI
	// webui is the path to the webUI archieve
	installWebUI: function( webui, callback){

	},

	// install a program
	// program is the path to the program archieve
	installProgram: function( program, callback ){

	},

	// delete the program
	deleteProgram: function( callback ){

	},

	// restart the program
	restartProgram: function( callback ){

	},


	/*
	 *	Revolt Hardware modules
	 */

	// get list of connected hardware modules
	getModuleList: function( callback ){

	}
}
