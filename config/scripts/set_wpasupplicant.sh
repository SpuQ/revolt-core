#!/bin/bash

#	set wpa supplicant
#	29/12/2016
#	usage:	sh set_wpasupplicant.sh <ssid> <psk>
#
#	note: 	only tested on RPi

# check whether script is run as root
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

SSID="$1"
PASSWD="$2"

FILE="/etc/wpa_supplicant/wpa_supplicant.conf"
DATE=$(date +"%d/%m/%Y")

# create wpa_supplicant.conf file
#echo -e $(echo "# generated by Revolt\n# on $DATE\n\nnetwork={\n\tssid=\"$SSID\"\n\tpsk=\"$PASSWD\"\n}") > "wpa_supplicant.conf"
echo -e $(echo "# generated by Revolt\n# on $DATE\n\nnetwork={\n\tssid=\"$SSID\"\n\tpsk=\"$PASSWD\"\n}") > $FILE

exit 0
