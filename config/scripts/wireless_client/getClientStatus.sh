
#!/bin/bash

#	check wireless client status
#	08/01/2017
#

# check whether script is run as root
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

if [ -z "$1" ]; then
	echo "first argument must be a wireless interface"
	exit 1;
fi

INTERFACE=$1
SSID=$(iwgetid | grep $INTERFACE);

if [ -n "$SSID" ]
then
	NETWORK=$(echo $SSID | cut -d '"' -f2);
	ADDRESS=$(ifconfig $INTERFACE | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}');

	printf "connected\n";
	printf "$NETWORK @ $ADDRESS"
else
	printf "disconnected";
fi

exit 0
