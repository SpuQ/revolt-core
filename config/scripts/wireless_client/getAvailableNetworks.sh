#!/bin/bash

#	List wireless networks
#	29/12/2016

# check whether script is run as root
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

if [ -z "$1" ]; then
	echo "first argument must be a wireless interface"
	exit 1;
fi

# first argument is interface
INTERFACE=$1

# list networks
iw dev $INTERFACE scan | grep SSID | awk '{ print $2 }'
