#!/bin/bash

#	deploy_appui
#	this scripts copies the file (argument) to the application ui directory
#	
#	note:	- this script assumes the file is a .tar archieve
#		- this script assumes to run from revolt's system directory
#	TODO this is dirty.


APPUIDIR="/var/revolt/appui/"
UPLOAD="upload.tar"

# change script execution to appui directory
cd $APPUIDIR

# remove everything in this directory
rm -rf *

# copy archive to application ui directory
mv $1 $UPLOAD

# untar archive here
tar -xf $UPLOAD

# remove archive
rm $UPLOAD

exit 0;
