/* 
 *	wirelessInterface.js
 *	NodeJS object for wireless network interfaces
 *
 *	written on 22/01/2017 by Tom 'SpuQ' Santens	
 */

var exec = require('child_process').exec;
var execSync = require('child_process').execSync;
var scriptsPath = "./scripts/wireless_client/";

module.exports = WirelessInterface;

function WirelessInterface( interfaceName ){
	// name must be set
	if(typeof interfaceName == "undefined") return -1;
	var name = interfaceName;

	var mode = "disabled";


	/*	setClient
	 *
	 */
	function setClient( settings ){
		console.log("setting "+name+" as client");
		console.log(JSON.stringify(settings));
		mode = "client";
		

		// TODO check settings for all data

		// TODO cleanup access point
		clearAccessPointNetwork();

		//TODO configure network
		setClientNetwork( settings );

		// configure interface (works!)
		setInterfaceConfiguration( settings );
	}

	this.setClient = setClient;


	/*	setAccessPoint
	 *
	 */
	function setAccessPoint( settings ){
		console.log("setting "+name+" as access point");
		console.log(JSON.stringify(settings));
		mode = "accessPoint";
		settings.static = true	// TODO this shouldn't be required to set interface for access point

		// TODO check settings for all data

		// configure interface (works!)
		setAccessPointInterfaceConfiguration( settings );
		// call access point script
		setAccessPointNetwork( settings );
	}

	this.setAccessPoint = setAccessPoint;


	/*	getClientStatus
	 *	27/01/2017 - ok?
	 *	
	 */
	function getClientStatus(){
		if(mode != "client"){
			return "inactive";
		}
		else {
			// execSync client status script
			var reply;
			try {
				reply = execSync("sh "+scriptsPath+"getClientStatus.sh "+name).toString();
			}
			catch(err){
				return "error while getting status";
			}

			return reply;
		}
	}

	this.getClientStatus = getClientStatus;


	/*	getAccessPointStatus
	 *
	 */
	function getAccessPointStatus(){
		if(mode != "accessPoint"){	
			return "inactive";
		}
		else {
			//TODO execSync client status script
			var reply;
			try {
				reply = execSync("sh "+scriptsPath+"getAccessPointStatus.sh "+name).toString();
			}
			catch(err){
				return "error while getting status";
			}

			return reply;
		}
	}

	this.getAccessPointStatus = getAccessPointStatus;


	/*	getInterfaceConfiguration
	 *	returns -1 on bad call (no callback)
	 *	calls the callback function with a json object containing the current configuration
	 *	{"address": "...", "netmask":"...", "gateway":"..."}
	 *	TODO make sync
	 */
	function getInterfaceConfiguration( callback ){
		if(typeof callback != "function") return -1;

		exec('sh '+scriptsPath+'getClientConfiguration.sh '+name, function(err, stdout, stderr){
			var configuration = {};
			var output = stdout.split('\n');

			if(err == null ){
				if(typeof output[0] != "undefined") configuration.address = output[0];
				if(typeof output[1] != "undefined") configuration.netmask = output[1];
				if(typeof output[2] != "undefined") configuration.gateway = output[2];
			}
			callback( configuration, err );
		});
	}

	this.getInterfaceConfiguration = getInterfaceConfiguration;


	/*	setInterfaceConfiguration
	 *	
	 *	TODO improve + documentation
	 */
	function setInterfaceConfiguration( settings ){
		if(typeof settings.static == "undefined") return -1;
		var reply;
		
		if(settings.static != true){
			// Dynamic configuration
			console.log("configuring "+name+" dynamically");
			try {
				reply = execSync("sh "+scriptsPath+"setInterfaceConfiguration.sh "+name).toString();
			}
			catch(err){
				console.log("error while configuring interface");
				return "error while setting interface (dynamic)";
			}
		}
		else {
			// Static configuration
			console.log("configuring "+name+" static with address: "+settings.address+" netmask: "+settings.netmask);
			if(typeof settings.address == "undefined") return -1;
			if(typeof settings.netmask == "undefined") return -1;

			try {
				reply = execSync("sh "+scriptsPath+"setInterfaceConfiguration.sh "+name+" "+settings.address+" "+ settings.netmask).toString();
			}
			catch(err){
				console.log("error while configuring interface");
				return "error while setting interface (static)";
			}
		}
		console.log("successfully configured interface");
		return reply;
	}

	this.setInterfaceConfiguration = setInterfaceConfiguration;


	/*
 	 *
	 */
	function setClientNetwork( settings ){
		var reply;
		if(settings.security != "none"){
			console.log("connecting to protected network (ssid: "+settings.ssid+", passwd: "+settings.passwd+")");
			try {
				reply = execSync("sh "+scriptsPath+"setClientNetwork.sh "+name+" "+settings.ssid+" "+ settings.passwd).toString();
			}
			catch(err){
				console.log("error while configuring client network");
				return "error";
			}
		}
		else{
			console.log("connecting to unprotected network (ssid: "+settings.ssid+")");
			try {
				reply = execSync("sh "+scriptsPath+"setClientNetwork.sh "+name+" "+settings.ssid).toString();
			}
			catch(err){
				console.log("error while configuring client network");
				return "error";
			}
		}
		console.log("success!");
		return reply;
	}


	/*	getAvailableNetworks
	 *	returns -1 on bad call
	 *	calls the callback function with an array of the available networks as argument
	 *	e.g. networks[{"ssid":"...","security":"..."}, ... ]
	 *	TODO: security (not urgent, not important)
	 */
	function getAvailableNetworks( callback ){
		if(typeof callback != "function") return -1;

		exec('sh '+scriptsPath+'getAvailableNetworks.sh '+name, function(err, stdout, stderr){
			var output = stdout.split('\n');
			var networks = [];

			if(err == null ){
				for(var i in output){
					if(output[i] == ''){
						output.splice(i,1);		// remove hidden networks from list
						break;
					}
					networks[i] = {};
					networks[i].ssid = output[i];
					networks[i].security = "fna";
				}
			}
			callback( networks, err );
		});
	}

	this.getAvailableNetworks = getAvailableNetworks;



	/*
	 *
	 */
	function setAccessPointNetwork( settings ){
		if(typeof settings.ssid == undefined) return -1;
		if(typeof settings.security == undefined) return -1;

		var reply;
		console.log("setting up Access Point");

		if( settings.security != "open" ){
			console.log("security");
			try {
				reply = execSync("sh "+scriptsPath+"setAccessPointNetwork.sh "+name+" "+settings.ssid+" "+settings.passwd).toString();
			}
			catch(err){
				console.log("error while configuring Access Point network");
				return "error";
			}
		}
		else {
			console.log("open");
			try {
				reply = execSync("sh "+scriptsPath+"setAccessPointNetwork.sh "+name+" "+settings.ssid).toString();
			}
			catch(err){
				console.log("error while configuring Access Point network");
				return "error";
			}
		}
		
		return reply;
	}

	/*
	 *
	 */
	function clearAccessPointNetwork(){
		var reply;
		console.log("cleaning up Access Point");
		try {
			reply = execSync("sh "+scriptsPath+"clearAccessPointNetwork.sh ").toString();
		}
		catch(err){
			console.log("error while clearing Access Point");
			return "error";
		}
		return reply;
	}


	/*	setInterfaceConfiguration
	 *	
	 *	TODO improve + documentation
	 */
	function setAccessPointInterfaceConfiguration( settings ){
		var reply;
		
		console.log("configuring "+name+" static with address: "+settings.address+" netmask: "+settings.netmask+" for AP");
		if(typeof settings.address == "undefined") return -1;
		if(typeof settings.netmask == "undefined") return -1;

		try {
			reply = execSync("sh "+scriptsPath+"setAccessPointInterfaceConfiguration.sh "+name+" "+settings.address+" "+ settings.netmask).toString();
		}
		catch(err){
			console.log("error while configuring interface for AP");
			return "error while setting interface (static) for AP";
		}
		console.log("successfully configured interface for AP");
		return reply;
	}

	this.setInterfaceConfiguration = setInterfaceConfiguration;
}
