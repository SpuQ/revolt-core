module.exports = function(RED) {
	console.log('registering Experimental "neural-network" node');


	function neuralNetwork_node(config){
        	RED.nodes.createNode(this, config);
		var node = this;
		var topic = config.topic;

		// function called on incoming msg
		this.on('input', function( msg ) {
			if(msg.topic == topic){

			}
		});

		// function called on application close
		this.on('close', function(done) {
			//TODO force off
			done();
		});

		status_indicator("error");	// set error by default

		// Status indicator beneath the node
		function status_indicator(status){
			if (status == "ok"){
				node.status({fill:"blue",shape:"dot",text:status});
			}
			else {
				node.status({fill:"red",shape:"dot",text:status});
			}
		}
	}

	RED.nodes.registerType("neural-network", neuralNetwork_node);
	console.log('Experimental "neural-network" node registered');
}
