/*
 *	POST
 */

module.exports = function(RED) {
	console.log('registering Revolt-system "rest-POST" node');
	Route = require('./rest');

	function rest_post(config){
        	RED.nodes.createNode(this,config);
		var node = this;
		var topic = config.topic;

		var data;

		var route = new Route(config.name, "POST", function( body ){
			var msg = {};
			msg.topic = topic;
			msg.payload = body;
			node.send(msg);
			return "post reply";
		});

		this.on('input', function( msg ) {
			// nothing...
		});

		this.on('close', function( done ){
			route.remove();
			done();
		});
	}

	RED.nodes.registerType("rest-POST", rest_post);
	console.log('Revolt-system "rest-POST" node registered');
}
