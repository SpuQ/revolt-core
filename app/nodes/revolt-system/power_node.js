module.exports = function(RED) {
	console.log('registering Revolt-system "power" node');

	// revolt system functions (same as config)
	var system = require('/opt/revolt/config/revolt-system.js');

	function revolt_power(config) {
		RED.nodes.createNode(this, config);
		var node = this;
		var topic = config.topic;
		
		status_indicator("idle");
		
		this.on('input', function(msg) {
			if(msg.topic == topic){
				if( msg.payload.power == "restart"){
					console.log("restarting system");
					system.restart();
				}
				else if( msg.payload.power == "shutdown"){
					console.log("shutting down system");
					system.shutdown();
				}
			}
		});

		function status_indicator(status){
			if (status == "idle"){
				node.status({fill:"blue",shape:"ring",text:"idle"});
			}
			else {
				node.status({fill:"red",shape:"dot",text:status});
			}
		}
	}

	RED.nodes.registerType("revolt-power", revolt_power);
	console.log('Revolt-system "power" node registered');
}
