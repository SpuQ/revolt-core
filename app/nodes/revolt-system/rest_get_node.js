/*
 *	GET
 */

module.exports = function(RED) {
	console.log('registering Revolt-system "rest-GET" node');
	Route = require('./rest');

	function rest_get(config){
        	RED.nodes.createNode(this,config);
		var node = this;

		var topic = config.topic;
		var bufferLast = config.bufferLast;		// buffer the last value
		var collect = config.collect;			// collect data in array
		var bufferSize = config.bufferSize;		// length of array
		var fifolifo = config.fifolifo;			// First in First out, or Last in First Out

		var data;		// unbuffered
		var buffer = [];

		var route = new Route(config.name, "GET", function( body ){
			if(bufferLast == "yes"){		// don't clear when fetched
				if( collect == "yes" ){
					return buffer;
				}
				else if( collect == "no" ){
					return data;
				}
			}
			else if(bufferLast == "no"){		// clear when fetched
				if( collect == "yes" ){
					var temp = {};
					temp = buffer;
					buffer = [];
					return temp;
				}
				else if( collect == "no" ){
					var temp;
					temp = data;
					data = "undefined";
					return temp;
				}
			}
		});

		this.on('input', function( msg ) {
			if(msg.topic == topic){
				if(collect == "no"){
					data = msg.payload;
				}
				else if(collect == "yes"){
					if(fifolifo == "fifo"){
						// add element to beginning of array
						buffer.unshift(msg.payload);
					}
					else if(fifolifo == "lifo"){
						// add element to end of array
						buffer.push(msg.payload);
					}
					// check array length - remove element
					if(buffer.length > bufferSize){
						if(fifolifo == "fifo"){
							// remove element form end of array
							buffer.pop();
						}
						else if(fifolifo == "lifo"){
							// remove element from beginning of array
							buffer.shift();
						}
					}
				}
			}
		});

		this.on('close', function( done ){
			route.remove();
			done();
		});
	}

	RED.nodes.registerType("rest-GET", rest_get);
	console.log('Revolt-system "rest-GET" node registered');
}
