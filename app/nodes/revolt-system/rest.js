/*
 *	rest.js
 *
 *	useful links:
 *	https://alexanderzeitler.com/articles/expressjs-dynamic-runtime-routing/
 */

// TODO restart server on deploy! - the close() thingy doesn't work... [known issue in NodeRED?]

/*
 *	Express webserver
 */

var express = require('express');
var app = express();
var appServer;
var bodyParser = require('body-parser');

app.use( bodyParser.json() );				// support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true }));	// support encoded bodies

// location of the application's webUI
app.use(express.static('/var/revolt/appui/'));

console.log("starting webserver");
appServer = app.listen(80);

module.exports = Route;

function Route(name, method, handler){
	var name = name;
	var method = method;

	console.log("adding route '/api/"+name+"' ["+method+"]");

	// GET
	if(method =="GET"){
		app.get('/api/'+name, function(req,res) {
			var data = handler(req.body);
			res.send(data);
		});
	}
	// POST
	else if( method =="POST"){
		app.post('/api/'+name, function(req,res){
			var data = handler(req.body);
			res.send(data);
		});
	}

	this.remove = function(){
		console.log("removing route '/api/"+name+"' ["+method+"]");

		//TODO remove route from stack; app._router.stack -> splice()
		//	app needs to be restarted now... not nice
		console.log(app._router.stack);
	}
}
