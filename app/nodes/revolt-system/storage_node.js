module.exports = function(RED) {
	console.log('registering Revolt-system "storage" node');

	Storage = require('./storage');

	function revolt_storage(config) {
		RED.nodes.createNode(this, config);
		var node = this;
		var topic = config.topic;

		var storage = new Storage(config.name);
		
		status_indicator("initializing");
		init();
		
		this.on('input', function(msg) {
			if(msg.topic == topic){
				status_indicator("writing");
				storage.write(msg.payload);
				node.send(msg);
				status_indicator("idle");
			}
		});

		function init(){
			storage.init();
			setTimeout(function(){
				var msg = {};
				msg.topic = topic;
				msg.payload = storage.read();
				node.send(msg);
				status_indicator("idle");
			}, 500);
		}

		function status_indicator(status){
			if (status == "idle"){
				node.status({fill:"blue",shape:"ring",text:"idle"});
			}
			else if(status == "writing"){
				node.status({fill:"blue",shape:"dot",text:"writing..."});
			}
			else {
				node.status({fill:"red",shape:"dot",text:status});
			}
		}
	}

	RED.nodes.registerType("storage", revolt_storage);
	console.log('Revolt-system "storage" node registered');
}
