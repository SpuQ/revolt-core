module.exports = function(RED) {
	function adaptor(config) {
		RED.nodes.createNode(this, config);
		var node = this;

		
		var sigin = config.sigin;
		var sigout = config.sigout;
		var valout = config.valout;

		this.on('input', function(msg) {
			if(msg.signal == sigin){
				msg.signal = sigout;
				if(valout != ""){
					msg.argument = valout.replace("*argument*", msg.argument);
				}
            			node.send(msg);
			}
		});
	}

	RED.nodes.registerType("adaptor",adaptor);
}
