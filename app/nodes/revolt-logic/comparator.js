module.exports = function(RED) {
	function comparator(config) {
		RED.nodes.createNode(this, config);
		var node = this;

		
		var sigin = config.sigin;
		var argin = config.argin;
	
		var sigequal = config.sigequal;
		var argequal = config.argequal;
		var sigbigger = config.sigbigger;
		var argbigger = config.argbigger;
		var sigsmaller = config.sigsmaller;
		var argsmaller = config.argsmaller;

		this.on('input', function(msg) {
			if(msg.signal == sigin){
				if (argin == msg.argument){
					if(argequal != ""){
						msg.argument = argequal.replace("*argument*", msg.argument);
					}
					if(sigequal != ""){
						msg.signal = sigequal;
            					node.send(msg);
					}
				}
				else if (argin < msg.argument){
					if(argbigger != ""){
						msg.argument = argbigger.replace("*argument*", msg.argument);
					}
					if(sigbigger != ""){
						msg.signal = sigbigger;
            					node.send(msg);
					}
				}
				else if (argin > msg.argument){
					if(argsmaller != ""){
						msg.argument = argsmaller.replace("*argument*", msg.argument);
					}
					if(sigsmaller != ""){
						msg.signal = sigsmaller;
            					node.send(msg);
					}
				}
			}
		});
	}

	RED.nodes.registerType("comparator",comparator);
}
