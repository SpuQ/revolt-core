/**
 * Copyright 2013, 2016 IBM Corp.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

// The `https` setting requires the `fs` module. Uncomment the following
// to make it available:
//var fs = require("fs");

module.exports = {
	// custom flow file (REVOLT)
	flowFile: "/var/revolt/flows/revolt_flow.js",

	// directory for additional nodes (REVOLT)
	nodesDir: "/opt/revolt/app/nodes/revolt_nodes/",

	// user root directory for node-red
	userDir: "/opt/revolt/app/",

	// the tcp port that the Node-RED web server is listening on
	uiPort: process.env.PORT || 1880,

	// Retry time in milliseconds for MQTT connections
	mqttReconnectTime: 15000,

	// Retry time in milliseconds for Serial port connections
	serialReconnectTime: 15000,

	// The maximum length, in characters, of any message sent to the debug sidebar tab
	debugMaxLength: 1000,

	functionGlobalContext: {

	},

    // Configure the logging output
    logging: {
        // Only console logging is currently supported
        console: {
            // trace - record very detailed logging + debug + info + warn + error + fatal errors
            level: "trace",
            // Whether or not to include metric events in the log output
            metrics: false,
            // Whether or not to include audit events in the log output
            audit: false
        }
    },

	editorTheme: {
		page: {
			title: "REVOLT - editor",
			favicon: "/opt/revolt/images/revolt_logo.png",
			css: "/opt/revolt/app/revolt.css"
		},

    		header: {
        		title: " ",
        		image: "/opt/revolt/images/revolt_scribble_black.png", // or null to remove image
        		url: "http://revolt.emcosys.be" // optional url to make the header text/image a link to this url
    		}
	},

	paletteCategories: ['subflows', 'Revolt_modules', 'Revolt_system', 'Revolt_logic', 'Experimental', 'function', 'input', 'output', 'social', 'analysis', 'storage', 'advanced']
}
