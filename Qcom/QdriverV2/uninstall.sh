#!/bin/sh

#	uninstall.sh
#	uninstall Qcom driver on Linux System
#	written by Tom 'SpuQ' Santens
#		on 27/09/2016
#
#	note:	- tested, works ;)

# check whether script is running as root
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

echo "Uninstalling Qcom"

# delete qcom user
userdel qcom

# delete Qcom directory '/Qcom/' and it's content
rm -rf /Qcom/

# delete '10-tarantula.rules' from /etc/udev/rules.d/
rm /etc/udev/rules.d/10-tarantula.rules

# restart udev service
service udev restart

echo "Qcom is uninstalled"
exit 0;
