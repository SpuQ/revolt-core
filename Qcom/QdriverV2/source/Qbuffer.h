#ifndef __QBUFFER_H__
#define __QBUFFER_H__

/*
 *	Qbuffer (V1)
 */

#include <string.h>

#ifndef BUFFERSIZE
	#define BUFFERSIZE 1024
#endif

/*
 * append: appends string to buffer
 * Returns 0 on succes, negative value on error.
*/
int _Qbuffer_append(char* buffer, char* str);

#endif
