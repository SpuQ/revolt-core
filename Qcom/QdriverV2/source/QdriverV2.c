/*
 *	Qcom device driver (for ttyUSB devices) Version 2
 *	written by Tom 'SpuQ' Santens
 *
 *	note:	- This driver expects the existence and full access to a folder /Qcom and /Qcom/devices/
 *
 * 	Directory Structure:
 *	[ ] /
 *	 |-[ ] Qcom
 *	 |  |- #  QdriverV2			(linux executable - this driver)
 *	 |  |- #  start_driver.sh		(start script, invoked on device connect)
 *	 |  |- #  Qcom.log			(logfile for all devices)
 *	 |  |-[ ] devices
 *	 |  |  |- <> deviceName_deviceAddress	(device sockets)
 *	    |  |- <> deviceName_deviceAddress
 *	       |- ...
 *
 *	license:	This piece of code is property of Tom 'SpuQ' Santens
 *			and is written for Emcosys' Cerebrium Machinae project
 *			TODO read and understand GPL license
 *
 */

#define DEBUG		// if DEBUG is defined, some helpful lines will be printed in stderr
//#define DEBUGLOG	// if DEBUGLOG is defined, debug lines will be printed in a logfile

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/socket.h>
#include <sys/un.h>

#include "Qpack.h"
#include "Qport.h"
#include "Qbuffer.h"

#define NAMESIZE 	30
#define BUFFERSIZE 	1024
#define QPACKSTRSIZE	128

#define RETRYATTEMPTS	10
#define INITSLEEPUS	500000
#ifndef DEBUG
#define MAINDELAYUS	100
#else
#define MAINDELAYUS	500000
#endif

#define QCOMLOGFILE	"Qcom.log"
#define CONNECTIONFILE	"connection"

#define QDEVNAMESIG	"device"
#define QDEVADDRSIG	"address"
#define QDEVSYSSIG	"SYS"

#define QCOMDIRECTORY	"/Qcom/"
#define QSOCKETDIR	"/Qcom/devices/"

#define	NOTIFYSIGNAL		"device"
#define	NOTIFYDISCONNECT	"disconnected"
#define	NOTIFYCONNECT		"connected"
#define NOTIFYERROR		"error"

/*	pre-processor macro's		*/
#define print_log(fmt, ...) 									\
        do { 	char logfilePath[NAMESIZE];							\
		sprintf(logfilePath,"%s%s",QCOMDIRECTORY, QCOMLOGFILE);				\
		FILE *fp = fopen(logfilePath,"a+"); 					\
			fprintf(fp,"%s @ %s:\t" fmt, socketName, devicePort, ##__VA_ARGS__ ); 	\
		fclose(fp); 									\
	} while (0)

// debugging
#ifdef DEBUGLOG
#define print_debug(...) 					\
	do {							\
		FILE *fp = fopen("/Qcom/debug.log","a+");	\
		fprintf(fp, __VA_ARGS__); 			\
		fclose(fp); 					\
	} while (0)
#endif

#ifdef DEBUG
#define print_debug(...) 					\
	do {							\
		fprintf(stderr, __VA_ARGS__); 			\
	} while (0)
#else
#define print_debug(...)
#endif

/*	function prototypes	*/
int driver_init(void);
int driver_exit(void);
int driver_createDeviceDir(void);
int driver_createStreams(void);

void subcycle_device(void);
void subcycle_ipc(void);

int ipc_init(void);
int ipc_exit(void);
int ipc_write(Qpack packet);
int ipc_syncInput(void);
int ipc_read(Qpack* packet);
int ipc_handler(Qpack packet);

int ipc_checkConnection(void);

int device_init(void);
int device_exit(void);
int device_write(Qpack packet);
int device_syncInput(void);
int device_read(Qpack* packet);
int device_handler(Qpack packet);

int device_checkConnection(void);
int device_requestName(void);
int device_requestAddress(void);
int device_checkForQcom(void);

int notify_device_disconnected(void);
int notify_device_connected(void);
int notify_device_error(void);


/*	global variables	*/
char devicePort[NAMESIZE];
char deviceName[NAMESIZE];
char deviceAddress[NAMESIZE];
char socketName[NAMESIZE];	// name of IPC unix domain socket

static char deviceBuffer[BUFFERSIZE];
static char ipcBuffer[BUFFERSIZE];

int deviceConnected = 0;	// device connection status
int clientConnected = 0;	// IPC client connetion status

int ipcSocket;
int ipcClientSocket;

struct stat st = {0}; // for the directory


int main(int argc, char** argv){
	// read which ttyUSB from program argument
	if( argc < 2 ){
		print_debug("MAIN: invalid program argument(s), exit.\n");
		return -1;	
	}
	sprintf(devicePort, "%s", argv[1]);
	print_debug("MAIN: starting driver for %s\n", devicePort);

	if( driver_init() ) {	// initialize Qcom Driver, exit if not successful
		print_debug("MAIN: driver initialization failed, exit.\n");
		return -1;
	}

	print_log("Qcom driver started.\n");
	print_debug("MAIN: driver initialized for '%s', starting main-loop\n", socketName);

	while(deviceConnected){
		print_debug("MAIN: device subcycle\n");
		subcycle_device();		// read from device and handle signals
		print_debug("MAIN: ipc subcycle\n");
		subcycle_ipc();			// read from ipc stream and handle signals

		print_debug("MAIN: sleep\n");
		usleep(MAINDELAYUS);		// sleep for some time - reduces CPU load? (usleep crashes on RPi?!)

		print_debug("MAIN: check connection\n");
		device_checkConnection();	
		ipc_checkConnection();		// added later
	}

	notify_device_disconnected();

	driver_exit();				// clean up Qcom Driver
	print_log("device disconnected, driver exit.\n");

	return 0;
}

int driver_init(void){
	*deviceName = '\0';
	*deviceAddress = '\0';

	*deviceBuffer = '\0';				// initialize buffers to null - could have used memset()
	*ipcBuffer = '\0';

	ipcSocket = 0;
	*socketName = '\0';

	if( device_init() ){				// Initialize Qcom device
		print_debug("DRIVER INIT: failed to initialize Qcom for %s\n", devicePort);
		return -1;
	}

	sprintf(socketName, "%s_%s", deviceName, deviceAddress);

	if (stat(QSOCKETDIR, &st) == -1) {		// check for existence of Qcom directory
   		print_debug("DRIVER INIT: Qcom directory not found (%s)\n", QSOCKETDIR);
		return -1;
	}
	chdir(QSOCKETDIR);				// change working directory to /Qcom
	print_debug("DRIVER INIT: driver now operating in %s\n", QSOCKETDIR);

	device_checkConnection();

	if( ipc_init() ){				// initialize IPC streams
		print_debug("DRIVER INIT: failed to initialize IPC");
		return -1;
	}

	return 0;
}

int driver_exit(void){
	print_debug("DRIVER EXIT: cleaning up device driver for %s @ %s\n", socketName, devicePort);
	device_exit();
	ipc_exit();
	print_debug("DRIVER EXIT: done. bye!\n");
	return 0;
}


/*	sub-cycles	*/

void subcycle_device(void){
	Qpack packet;

	device_syncInput();				// get buffered data from device

	while( !device_read( &packet ) ){		// while we keep getting Qcom packages from the device buffer
		if( device_handler( packet ) ){		// deliver data to the right place
			print_debug("IPC SUBCYCLE: some data was not handled...\n");
		}
	}
}

void subcycle_ipc(void){
	struct sockaddr_un remote;
	int t = sizeof(remote);
	int n;
	Qpack packet;
	
	if( !clientConnected ){				// if there is no client connected, check for incoming connections and accept if any
		t = sizeof(remote);

		ipcClientSocket = accept(ipcSocket, (struct sockaddr *)&remote, &t);
		if ( ipcClientSocket == -1) {
			print_debug("IPC SUBCYCLE: no connections waiting...\n");
		}
		else {
			print_debug("IPC SUBCYCLE: client accepted!\n");
			clientConnected = 1;
			print_log("application connected\n");
			notify_device_connected();	// probably misplaced here - make sure system gets the message?
		}
	}
	else {						//else read from client
		ipc_syncInput();
		while( !ipc_read( &packet ) ){		// while we keep getting Qcom packages from the device buffer
			if( ipc_handler( packet ) ){
				print_debug("IPC SUBCYCLE: some data was not handled...\n");
			}
		}
	}
}


/*	IPC socket
 *	Everything that has to do with the IPC socket
 */

int ipc_init(void){
	// initialize socket
	int len;
	struct sockaddr_un local;

    	if ((ipcSocket = socket(AF_UNIX, SOCK_STREAM | SOCK_NONBLOCK, 0)) == -1) {
		perror("socket");
        	return -1;
    	}

	local.sun_family = AF_UNIX;
	strcpy(local.sun_path, socketName);
	unlink(local.sun_path);
	len = strlen(local.sun_path) + sizeof(local.sun_family);

	if (bind(ipcSocket, (struct sockaddr *)&local, len) == -1) {
		print_debug("IPC INIT: error while binding socket\n");
		return -1;
	}

	if (listen(ipcSocket, 1) == -1) {
		print_debug("IPC INIT: error while listen socket\n");
		return -1;
	}

	print_debug("IPC INIT: socket initialized\n");
	return 0;
}

int ipc_exit(void){
	close(ipcClientSocket);				// close socket
	print_debug("IPC EXIT: socket closed\n");

	remove(socketName);				// delete socket from file system
	print_debug("IPC EXIT: socket cleaned up\n");
	return 0;
}

int ipc_write(Qpack packet){
	if( !packet.signal ) return -1;

	if( clientConnected ){
		char packStr[QPACKSTRSIZE];
		_Qpack_pack_to_jsonstr(&packet, packStr);			// stringify Qpack
		print_debug("IPC WRITE: writing '%s' to %s\n", packStr, socketName);

		int sendstat = send(ipcClientSocket, packStr, strlen(packStr), MSG_NOSIGNAL);	// send - "no signal"-flag: streams

		if ( sendstat < 0 ) {							// on error
			print_debug("IPC WRITE: send error: %s\n", strerror(errno) );
			
			if( !close(ipcClientSocket) ){			// if send fails, client is probably disconnected, so close
				print_debug("IPC WRITE: failed to close socket\n");
				return -1;
			}

			clientConnected = 0;
			print_log("application disconnected\n");
			
			return -1;
		}
	}
	else{
		print_debug("IPC WRITE: no client, data dropped\n");
		return -1;
	}

	print_debug("IPC WRITE: data written to %s\n", socketName);
	return 0;
}

int ipc_syncInput(void){
	char readBuffer[QPACKSTRSIZE];
	int n=0;

	print_debug("IPC SYNC: starting\n");

	n = recv(ipcClientSocket, readBuffer, QPACKSTRSIZE-2,MSG_ERRQUEUE | MSG_DONTWAIT);	// read data from socket - "don't wait"-flag!

	if (n < 0){
		if( errno != EAGAIN ){	// in non-blocking, EAGAIN happens all the time, so ignore
			print_debug("IPC SYNC: recv error: %s\n", strerror( errno ) );

			if( !close(ipcClientSocket) ){			// if recv fails, client is probably disconnected
				print_debug("IPC SYNC: failed to close socket\n");
				return -1;
			}
			clientConnected = 0;
			print_log("application disconnected\n");
			return -1;
		}
		n = 0;
	}

	if(n == 0){
		print_debug("IPC SYNC: no data from socket\n");
		 return 0;			// no data; no use to continue
	}

	// on success, append data to local buffer
	readBuffer[n]='\0';
	print_debug("IPC SYNC: data from socket: %s\n", readBuffer);

	if( _Qbuffer_append(ipcBuffer, readBuffer) ){
		print_debug("IPC SYNC: not enough space in buffer, data not written\n");
		return -1;
	}

	print_debug("IPC SYNC: buffer synchronized:\n%s\n", ipcBuffer);
	return 0;


}

int ipc_read(Qpack* packet){
	if( _Qpack_get_from_buffer_json(ipcBuffer, packet) ){
		print_debug("IPC READ: no JSON Qpacks in buffer\n");
		return -1;
	}

	print_debug("IPC READ: got Qpack out: %s - %s\n", (*packet).signal, (*packet).argument);
	return 0;
}

int ipc_handler(Qpack packet){
	if( !strcmp(packet.signal, "exit") ){
		// disconnect ipc
		print_log("application disconnected (exit signal)\n");
		close(ipcClientSocket);
		clientConnected = 0;
	}
	else{
		if( device_write(packet) ){	// send signal to device
			print_debug("IPC HANDLER: failed to write packet to device\n");
			return -1;
		}
	}
	return 0;
}

int ipc_checkConnection(){
	Qpack packet;
	strcpy(packet.argument, "?");
	strcpy(packet.signal, "ping");	// set signal to check connection
	ipc_write(packet);
	return 0;
}

/*	device		
 *	Everything that has to do with communication to the hardware device (serial port)
 */

int device_init(void){
	if( _Qport_init(devicePort) ){		// initialize serial port
		print_debug("DEVICE INIT: could not initialize comport for %s\n", devicePort);
		return -1;
	}

	if( device_checkForQcom() ){
		print_debug("DEVICE INIT: Device doesn't speak Qcom\n");
		return -1;
	}

	return 0;
}

int device_exit(void){
	_Qport_close();		// close comport
	return 0;
}

int device_write(Qpack packet){
	if( !packet.signal ) return -1;		// check for valid packet

	char packStr[QPACKSTRSIZE];

	if( _Qpack_pack_to_string(&packet, packStr) ){	// stringify Qpack
		print_debug("DEVICE WRITE: failed to stringify Qpack\n");
		return -1;
	}

	print_debug("DEVICE WRITE: writing: %s\n", packStr);

	if( _Qport_write(packStr) ){			// write to device
		print_debug("DEVICE WRITE: failed to write to device\n");
		return -1;
	}

	return 0;
}

int device_syncInput(void){
	char readBuffer[QPACKSTRSIZE];
	print_debug("DEVICE SYNC: buffer before sync: %s\n", deviceBuffer);

	if( _Qport_read(readBuffer, QPACKSTRSIZE) ){	// limited read comport buffer
		print_debug("DEVICE SYNC: failed to read from Qport\n");
		return -1;		
	}

	print_debug("DEVICE SYNC: from device: %s\n", readBuffer);

	if( _Qbuffer_append(deviceBuffer, readBuffer) ){
		print_debug("DEVICE SYNC: not enough space in buffer, data not written\n");
		return -1;
	}

	print_debug("DEVICE SYNC: buffer after sync: %s\n", deviceBuffer);
	return 0;
}

int device_read(Qpack* packet){
	print_debug("DEVICE READ: buffer before:\n%s\n", deviceBuffer);

	if( _Qpack_get_from_buffer(deviceBuffer, packet) ){
		print_debug("DEVICE READ: no packets in buffer\n");
		return -1;
	}

	print_debug("DEVICE READ: buffer after:\n%s\n", deviceBuffer);
	return 0;
}

int device_handler(Qpack packet){
	if( !packet.signal ) return -1;			// first, check whether there's a signal

	if( !strcmp(packet.signal, QDEVSYSSIG) ){	// write device messages to device log
		print_debug("DEVICE HANDLER: System message from device: %s\n", packet.argument);
		print_log("%s\n", packet.argument);
		notify_device_error();
	}

	else if( !strcmp(packet.signal, QDEVADDRSIG) ){	// set device address
		strcpy(deviceAddress, packet.argument);
	}
	else if( !strcmp(packet.signal, QDEVNAMESIG) ){	// set device name
		strcpy(deviceName, packet.argument);
	}

	else {
		if( ipc_write(packet) ){		// write device signals app
			print_debug("DEVICE HANDLER: failed to send data to IPC\n");
			return -1;
		}
	}

	return 0;
}

int device_checkConnection(void){
	int prevDeviceConnected = deviceConnected;
	char devPath[40];

	sprintf(devPath, "/dev/%s", devicePort);
	
	print_debug("DEVICE CONNECTION: checking device connection\n");

	if( (access( devPath, F_OK ) >= 0)) {
		deviceConnected = 1;
		print_debug("DEVICE CONNECTION: all access to device file\n");
	}
	else{
		deviceConnected = 0;
		print_debug("DEVICE CONNECTION: no access to device file, device disconnected\n");
	}

	return 0;
}

/*	Qdevice stuff	*/

int device_requestName(void){
	Qpack packet;
	strcpy(packet.argument, "?");
	strcpy(packet.signal, QDEVNAMESIG);	// set signal to name-signal
	device_write(packet);
	return 0;
}

int device_requestAddress(void){
	Qpack packet;
	strcpy(packet.argument, "?");
	strcpy(packet.signal, QDEVADDRSIG);	// set signal to address-signal
	device_write(packet);
	return 0;
}

int device_checkForQcom(void){
	int i=0;

	while( *deviceName == '\0'){
		i++;
		if(i>RETRYATTEMPTS){
			print_debug("driver exit: no name received from device\n");
			return -1;
		}
		print_debug("request for device name: attempt %d\n", i);
		device_requestName();

		usleep(INITSLEEPUS);
		subcycle_device();
		//usleep(INITSLEEPUS);
	}

	i=0;

	while(*deviceAddress == '\0'){
		i++;
		if(i>RETRYATTEMPTS){
			print_debug("driver exit: no address received from device\n");
			return -1;
		}
		print_debug("request for device address: attempt %d\n", i);
		device_requestAddress();

		usleep(INITSLEEPUS);
		subcycle_device();
		//usleep(INITSLEEPUS);
	}

	print_debug("Device name: '%s'\n", deviceName);
	print_debug("Device address: '%s'\n", deviceAddress);

	return 0;
}

/*	status messages etc
 *	Notify applications on events like device disconnect
 */

int notify_device_disconnected(void){
	Qpack packet;
	strcpy(packet.argument, NOTIFYDISCONNECT);
	strcpy(packet.signal, NOTIFYSIGNAL);

	ipc_write(packet);
}

int notify_device_connected(void){
	Qpack packet;
	strcpy(packet.argument, NOTIFYCONNECT);
	strcpy(packet.signal, NOTIFYSIGNAL);

	ipc_write(packet);
}

int notify_device_error(void){
	Qpack packet;
	strcpy(packet.argument, NOTIFYERROR);
	strcpy(packet.signal, NOTIFYSIGNAL);

	ipc_write(packet);
}

