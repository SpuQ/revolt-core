#!/bin/sh

#	install.sh
#	install Qcom driver on Linux System
#	written by Tom 'SpuQ' Santens
#		on 27/09/2016
#
#	note:	- run this script as root from folder where this script is
#		- it should work; I've written worse scripts ;D

# check whether script is running as root
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

echo "Installing Qcom"

cd ./Qcom/QdriverV2/

# create qcom user
useradd -r -s /bin/false qcom

# create Qcom directory '/Qcom/'
mkdir /Qcom/

# create directory for sockets '/Qcom/devices'
mkdir /Qcom/devices/

# compile Qcom driver
make -C ./source/

# move Qdriver to /Qcom/ directory
mv ./source/QdriverV2 /Qcom/

# copy '10-tarantula.rules' to /etc/udev/rules.d/
cp ./other/10-tarantula.rules /etc/udev/rules.d/

# restart udev service
service udev restart

# copy 'start_driver.sh' to /Qcom/ directory
cp ./other/start_driver.sh /Qcom/

# change ownership of /Qcom/ and it's content to qcom user
chown -R qcom:qcom /Qcom/

echo "Qcom is installed"
exit 0;
