/*
 *	app.js
 *	test/example node.js application for QcomV2.js
 *
 *	written by:	Tom 'SpuQ' Santens
 *		on:	27/09/2016
 */


var Qdevice = require('./QcomV2');

var display = new Qdevice("pe_display_1", function(arg){
						console.log( JSON.stringify(arg) );
					});

var sensor = new Qdevice("es_lightsensor_1", function(arg){
						console.log( JSON.stringify(arg) );
					});

main();

function main(){
	console.log(  Qdevice.deviceList );
	console.log("starting main loop");

	//for(;;){
		displayf();
		sensorf();
	//}
}

function displayf(){
		var msg = {};
		msg.signal = "ln_1";
		msg.argument = "blub";
		display.send( msg );
}

function sensorf(){
		var msg = {};
		msg.signal = "ln_1";
		msg.argument = "blub";
		sensor.send( msg );
}

