/*
 *	QcomV2.js
 *	NodeJS driver for QcomV2 (QdriverV2)
 *	based on QcomV1 driver (qcom.js)
 *
 *	written by:	Tom 'SpuQ' Santens
 *		on:	27/09/2016
 *
 *	helpful resources: https://nodejs.org/api/net.html
 * usage:
 *	var sensor = new Qdevice("es_lightsensor_1", function( signal ) {
 *						...
 *					});
 */

var net = require('net');
var fs = require('fs');
var readline = require('readline');

var socketDir = "/Qcom/devices/"

module.exports = Qdevice;
module.exports.deviceList = getDeviceList();

// Qdevice object constructor
function Qdevice( name , handler ){
	var deviceName = name;
	var socketPath;
	var socketLineIn;
	var socketClient;

	var inputHandler = handler;

	console.log("constructing Qdevice for '"+deviceName+"'");

	socketPath = socketDir+deviceName;
	console.log(socketPath);

	connect();

	function connect(){
		socketClient = net.createConnection(socketPath);

		socketLineIn = readline.createInterface( { terminal : false, input: socketClient } );


		socketLineIn.on('line', function(line){
			//console.log("<- "+deviceName+": "+ line);

			var obj = JSON.parse(line);
			if(typeof(inputHandler) == "function"){		// check whether function is set
				inputHandler( obj );
			}
		});

		socketClient.on("error", function(err){
			if(err.code == "ECONNREFUSED"){
				//console.log("connection refused");
			}
		});

		socketClient.on("close", function(){
			//console.log("connection closed by server");
			reconnect();
		});


	}
	
	this.send = function( jsonmsg ) {
		//console.log("-> "+deviceName+": "+ JSON.stringify(jsonmsg));
		if( jsonmsg.signal ){
			socketClient.write( JSON.stringify(jsonmsg) );
		}
	}

	function reconnect(){
		setTimeout(connect, 1000);
	}
}

// get the device list
function getDeviceList(){
	devices = {};

	// TODO scan Qcom directory for sockets
	devices = fs.readdirSync(socketDir);
	return devices;
}

