#!/bin/sh

#	This script should be called from /etc/udev/rules.d/ when a new FT232 device is connected
#	e.g.: RUN+="/bin/sh /Qcom/start_driver.sh '%E{DEVNAME}' "
#


# get ttyUSBx from argument
DPATH=$1
DEVFILE=$(echo ${DPATH##*/})

# start Qdriver
cd /Qcom/
./QdriverV2 $DEVFILE &
#valgrind ./QdriverV2 $DEVFILE > /Qcom/$DEVFILE.valgrind 2>&1 &

exit 0;
