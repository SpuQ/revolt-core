#!/bin/sh

#	backup.sh
#	backup the whole QcomV2 directory
#	written by Tom 'SpuQ' Santens
#		on 27/09/2016

echo "Backing up QcomV2";
echo $1;
NOW=$(date +"%Y%m%d-%H%M")
ARCHIVE_NAME=${PWD##*/}"_""$NOW""_""$1";
echo "creating archive $ARCHIVE_NAME";
tar cvf $ARCHIVE_NAME.tar .
echo "done"

exit 0;
