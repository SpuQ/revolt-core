#!/bin/sh

#	install.sh
#	install Qcom driver on Linux System
#	written by Tom 'SpuQ' Santens
#		on 27/09/2016
#		reviewed:	10/12/2016 - changes for first Laika installer
#
#	note:	- run this script as root from folder where this script is
#		- it should work; I've written worse scripts ;D

# check whether script is running as root
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

echo "++ Installing Qcom"

# change directory to the directory of this script - paths are relative
echo "++ changing working directory to $(dirname "$0")";
cd "$(dirname "$0")"

# create qcom user
echo "+ creating Qcom user"
useradd -r -s /bin/false qcom

# create Qcom directory '/Qdev/' in /
echo "+ creating Qdev directory in root for device files"
mkdir /Qdev/

echo "+ creating Qcom directory in /opt/ to hold device driver"
mkdir /opt/Qcom/

# copy the Qcom source code to /opt/Qcom/src/
echo "+ copying Qdriver source code to /opt/Qcom/src/"
mkdir /opt/Qcom/src/
cp ./source/* /opt/Qcom/src/

# compile Qcom driver
echo "+ compiling the Qcom driver"
make -C ./source/

# move Qdriver to /Qcom/ directory
echo "+ moving driver binary to /opt/Qcom/"
mv ./source/QdriverV2 /opt/Qcom/

# copy 'start_driver.sh' to /Qcom/ directory
echo "+ moving driver start scripts to /opt/Qcom/"
cp ./other/start_driver.sh /opt/Qcom/
cp ./other/actual_start.sh /opt/Qcom/

# put uninstall script in /opt/Qcom/
echo "+ moving uninstall script to /opt/Qcom/"
cp uninstall.sh /opt/Qcom/

# copy '10-tarantula.rules' to /etc/udev/rules.d/
echo "+ installing udev rules for Qcom devices"
cp ./other/10-tarantula.rules /etc/udev/rules.d/

# restart udev service
echo "+ restarting udev service"
service udev restart

# change ownership of /Qdev/ and it's content to qcom user
echo "+ changing ownership of /Qdev/ and its content to Qcom user"
chown -R qcom:qcom /Qdev/

# add Qcom service (only creates /dev/Qcom directory on startup, for now)
echo "+ adding Qcom service to /etc/init.d"
cp ./other/Qcom /etc/init.d/
update-rc.d Qcom defaults

echo "++ Qcom is installed"
exit 0;
