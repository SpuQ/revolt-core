#!/bin/bash

#	This script should be called from /etc/udev/rules.d/ when a new FT232 device is connected
#	e.g.: RUN+="/bin/sh /Qcom/start_driver.sh '%E{DEVNAME}' "
#


# get ttyUSBx from argument
DPATH=$1
DEVFILE=$(echo ${DPATH##*/})

# call actual start script and disown - this script and its children get killed after
# a while by systemd-udevd ("RUN can only be used for short tastks" - askubuntu.com),
# so we must call an other script and make it an orphan in order for it to survive...

#/bin/bash actual_start.sh $DEVFILE & disown

#echo "/opt/Qcom/actual_start.sh $DEVFILE" | at now

nohup /opt/Qcom/actual_start.sh $DEVFILE &

exit 0;
