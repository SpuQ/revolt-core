#!/bin/bash

#	This script is called by the script that is called by the udev rules
#


# get ttyUSBx from argument
DEVFILE=$1

# start Qdriver
#/opt/Qcom/QdriverV2 $DEVFILE >> /dev/null 2>&1 & disown

# this solution requires "at" - not installed by default
echo "/opt/Qcom/QdriverV2 $DEVFILE >> /dev/null 2>&1" | at now &
exit 0;
