#!/bin/sh

#	uninstall.sh
#	uninstall Qcom driver on Linux System
#	written by Tom 'SpuQ' Santens
#		on 27/09/2016
#		review	10/12/2016 - laika

# check whether script is running as root
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

echo "++ Uninstalling Qcom"

# delete qcom user
echo "+ deleting Qcom user"
userdel qcom

# delete Qcom directory '/Qcom/' and its content
echo "+ deleting /Qdev/ directory and its content"
rm -rf /Qdev/

# delete logfiles
echo "+ deleting debug and logfiles from /var/log/"
rm /var/log/Qcom.log
rm /var/log/Qcom.debug

# delete '10-tarantula.rules' from /etc/udev/rules.d/
echo "+ deleting udev rules for Qcom devices"
rm /etc/udev/rules.d/10-tarantula.rules

# delete /opt/Qcom/ directory
echo "+ deleting /opt/Qcom/ directory and its content"
rm -rf /opt/Qcom/

# restart udev service
echo "+ restarting udev service"
service udev restart

echo "+ removing Qcom service from /etc/init.d"
rm /etc/init.d/Qcom
update-rc.d Qcom remove

echo "++ Qcom is uninstalled"
exit 0;
