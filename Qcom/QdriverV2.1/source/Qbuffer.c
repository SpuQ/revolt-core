#include "Qbuffer.h"

#include <string.h>

int _Qbuffer_append(char* buffer, char* str){
	if( !str ) return -1;				// if str is NULL, exit
	if( !buffer) return -1;				// if buffer is NULL, exit

	int bufferspace = BUFFERSIZE - sizeof(buffer);	// calculate the remaining space in buffer
	int strsize = sizeof(str);			// check size of the string

	if(bufferspace >= strsize){			// if string fits in buffer, append it to the buffer
		strcat(buffer, str);
		return 0;
	}

	return -1;
}
