#include <stdio.h>
#include <string.h>

#include "Qpack.h"

int main(void){
	Qpack pack;
	char str[1024];


	fprintf(stderr, "# normal operation\n");

	fprintf(stderr, "pack to string: \n");
	strcpy(pack.signal, "blub");
	strcpy(pack.argument, "zeemeermin");
	if( _Qpack_pack_to_string(&pack, str) ){
		fprintf(stderr, "function returned error\n");
	}
	fprintf(stderr, "output: %s\n", str);

	fprintf(stderr, "string to pack: \n");
	strcpy(str, "{heerst:over}");
	if( _Qpack_string_to_pack(str, &pack) ){
		fprintf(stderr, "function returned error\n");
	}
	fprintf(stderr, "output: %s %s\n", pack.signal, pack.argument);

	fprintf(stderr, "pack to JSON string: \n");
	strcpy(pack.signal, "land");
	strcpy(pack.argument, "en");
	if( _Qpack_pack_to_jsonstr(&pack, str) ){
		fprintf(stderr, "function returned error\n");
	}
	fprintf(stderr, "output: %s\n", str);

	fprintf(stderr, "JSON string to pack: \n");
	strcpy(str, "{\"signal\":\"zee\",\"argument\":\"enzo\"}");
	fprintf(stderr, "JSON string: %s\n", str);
	if( _Qpack_jsonstr_to_pack(str, &pack) ){
		fprintf(stderr, "function returned error\n");
	}
	fprintf(stderr, "output: %s %s\n", pack.signal, pack.argument);


	/*	bad input	*/
	fprintf(stderr, "# bad input\n");

	fprintf(stderr, "pack to JSON string: \n");
	strcpy(pack.signal, "land");
	strcpy(pack.argument, "en");
	if( _Qpack_pack_to_jsonstr(&pack, str) ){
		fprintf(stderr, "function returned error\n");
	}

	fprintf(stderr, "output: %s\n", str);

	fprintf(stderr, "JSON string to pack - too many fields\n");
	strcpy(str, "{\"signal\":\"zee\",\"argument\":\"enzo\",\"bam\":\"boem\"}");
	fprintf(stderr, "JSON string: %s\n", str);
	if( _Qpack_jsonstr_to_pack(str, &pack) ){
		fprintf(stderr, "function returned error\n");
	}
	fprintf(stderr, "output: %s %s\n", pack.signal, pack.argument);


	fprintf(stderr, "done\n");

	return 0;
}
