#ifndef __QPACK_H__
#define __QPACK_H__

/*
 *	Qpack (V1)
 * 	contains definitions of the Qcom (V1) format, and some operations to get them from or put them somewhere.
 *
 *	note:	- this whole thing could have been written cleaner. One day, someone please do so...
 */

#include <stdio.h>
#include <string.h>

#define SIGNALSIZE	40
#define ARGUMENTSIZE	40

#define STARTBYTE	'{'
#define STOPBYTE	'}'
#define SEPARATOR	':'

typedef struct{
		char signal[SIGNALSIZE];
		char argument[ARGUMENTSIZE];
		} Qpack;

/*
 * Converts a Qpack into a string.
 * Returns 0 on succes, negative value on error.
*/
int _Qpack_pack_to_string(Qpack* pack, char* str);

/*
 * Converts a string into a Qpack.
 * Returns 0 on success, negative value on error.
*/
int _Qpack_string_to_pack(char* str, Qpack* pack);

/*
 * Converts a Qpack into a JSON formatted string.
 * Returns 0 on succes, negative value on error.
*/
int _Qpack_pack_to_jsonstr(Qpack* pack, char* jsonstr);

/*
 * Converts a JSON formatted string into a Qpack.
 * Returns 0 on success, negative value on error.
*/
int _Qpack_jsonstr_to_pack(char* jsonstr, Qpack* pack);

/*
 * Gets and removes the first JSON fromatted package out of a string.
 * Returns 0 on success, negative value on error.
*/
int _Qpack_get_from_buffer_json(char* buffer, Qpack* pack);

/*
 * Gets and removes the first package out of a string.
 * Returns 0 on success, negative value on error.
*/
int _Qpack_get_from_buffer(char* buffer, Qpack* pack);

/*
 * Prints Qpack in stdout
*/
void _Qpack_print_pack(Qpack* pack);

#endif
