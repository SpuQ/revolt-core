#!/bin/sh

# Script to start Revolt
# note: changed 10/12/2016 - little bit obsolete

# check whether script is running as root
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

echo "Starting Revolt"

#echo "+ starting system stuff (forever)"
#/usr/local/bin/forever ./system/system.js &

echo "+ starting system"
#/usr/bin/nodejs ./system/system.js >> system.log 2>&1 &
/etc/init.d/revolt start

echo "+ starting NodeRED"
#/usr/bin/node-red >> system.log 2>&1 &
/etc/init.d/node-red start

echo "done"
exit 0;
