#!/bin/sh

# check whether script is running as root
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

echo "terminating Revolt"

echo "+ killing system stuff"
#killall forever
#killall nodejs
/etc/init.d/revolt stop

echo "+ killing NodeRED"
#killall node-red
/etc/init.d/node-red stop

echo "done"
exit 0;
